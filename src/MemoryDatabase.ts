import {
    CommonDatabaseConditionBuilder,
    CommonDatabaseConditionParser,
    Database,
    DatabaseConditionBuilder,
    DatabaseConditionParser,
    DatabaseModel,
    DatabaseQuery,
    Repository,
} from "@thorbens/database";
import {MemoryRepository} from "./MemoryRepository";
import {RepositoryConstructor} from "@thorbens/database/dist/Database";

/**
 * Database implementation for a memory database.
 */
export class MemoryDatabase implements Database {
    /**
     * A map which contains already created repositories.
     * The key maps the repository name to a repository.
     */
    private readonly repositoryMap: Map<string, Repository<any>> = new Map();
    /**
     * The condition builder used.
     */
    private readonly conditionBuilder = new CommonDatabaseConditionBuilder();
    /**
     * The condition parser used.
     */
    private readonly conditionParser = new CommonDatabaseConditionParser();

    public async getRepository<T extends DatabaseModel>(type: RepositoryConstructor<T>): Promise<Repository<T>> {
        const className = type.name;
        // check if repository has been created already
        if (this.repositoryMap.has(className)) {
            return this.repositoryMap.get(className) as Repository<T>;
        }
        // create new repository
        const repository = new MemoryRepository<T>();
        this.repositoryMap.set(className, repository);

        return repository;
    }

    public async dropRepository<T extends DatabaseModel>(type: new (...args: any[]) => T): Promise<void> {
        this.repositoryMap.delete(type.name);
    }

    public async connect(): Promise<void> {
        // no op
    }

    public async disconnect(): Promise<void> {
        // no op
    }

    public createQuery<T>(): DatabaseQuery<T> {
        throw new Error(`not supported`);
    }

    public getConditionBuilder<TModel>(): DatabaseConditionBuilder<TModel> {
        return this.conditionBuilder;
    }

    public getConditionParser<TModel>(): DatabaseConditionParser<TModel> {
        return this.conditionParser;
    }
}
