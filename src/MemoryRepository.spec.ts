import {DatabaseModel} from "@thorbens/database";
import {MemoryDatabase} from "./MemoryDatabase";

class MockModel extends DatabaseModel {
    public foo?: string;
}

type ProjectionModel = Pick<MockModel, "foo">;

describe("MemoryRepository", () => {
    it("should insert a new element and retrieve it", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository(MockModel);
        const mockElement: MockModel = {
            id: `foo`,
        };
        // ensure that the element is not in the database
        const foundElement = await repository.findById(mockElement.id);
        expect(foundElement).toEqual(null);

        await repository.insertOne(mockElement);
        const afterInsertFoundElement = await repository.findById(mockElement.id) as MockModel;
        expect(afterInsertFoundElement).not.toEqual(null);
        expect(afterInsertFoundElement.id).toEqual(mockElement.id);
    });

    it("should insert new elements and retrieve it", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository(MockModel);
        const ids = [`first`, `second`, `third`];
        const mockElements: MockModel[] = ids.map(id => ({
            id,
        }));
        // ensure that no element is in the database
        for (const id of ids) {
            const foundElement = await repository.findById(id);
            expect(foundElement).toEqual(null);
        }

        await repository.insert(mockElements);
        for (const id of ids) {
            const afterInsertFoundElement = await repository.findById(id) as MockModel;
            expect(afterInsertFoundElement).not.toEqual(null);
            expect(afterInsertFoundElement.id).toEqual(id);
        }
    });

    it("should replace an existing element", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository(MockModel);
        const mockElement: MockModel = {
            id: `foo`,
            foo: `bar`,
        };
        await repository.insertOne(mockElement);
        const newMockElement: MockModel = {
            id: `foo`,
            foo: `boz`,
        };
        await repository.replaceOne(newMockElement);
        const afterInsertFoundElement = await repository.findById(mockElement.id) as MockModel;
        expect(afterInsertFoundElement).not.toEqual(null);
        expect(afterInsertFoundElement.id).toEqual(newMockElement.id);
        expect(afterInsertFoundElement.id).toEqual(newMockElement.id);
    });

    it("should update one field of an existing element", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository(MockModel);
        const mockElement: MockModel = {
            id: `foo`,
            foo: `bar`,
        };
        await repository.insertOne(mockElement);
        await repository.updateOneField(mockElement.id, `foo`, `baz`);
        const afterInsertFoundElement = await repository.findById(mockElement.id) as MockModel;
        expect(afterInsertFoundElement).not.toEqual(null);
        expect(afterInsertFoundElement.id).toEqual(mockElement.id);
        expect(afterInsertFoundElement.foo).toEqual(`baz`);
    });

    it("should find the correct element by the given equal query", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository(MockModel);
        const ids = [`first`, `second`, `third`];
        const mockElements: MockModel[] = ids.map(id => ({
            id,
        }));
        await repository.insert(mockElements);

        const conditionBuilder = database.getConditionBuilder<MockModel>();
        const query = repository.createQuery();
        const wantedId = "second";
        query.addCondition(conditionBuilder.createEqualOperator("id", wantedId));

        const result = (await repository.findByQuery(query)).getResults();
        expect(result.length).toEqual(1);
        expect(result[0].id).toEqual(wantedId);
    });

    it("should find the correct element by the given regexp query", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository(MockModel);
        const ids = [`first`, `second`, `third`];
        const mockElements: MockModel[] = ids.map(id => ({
            id,
        }));
        await repository.insert(mockElements);

        const conditionBuilder = database.getConditionBuilder<MockModel>();
        const query = repository.createQuery();
        query.addCondition(conditionBuilder.createRegExpOperator("id", /ir/i));

        const result = (await repository.findByQuery(query)).getResults();
        expect(result.length).toEqual(2);
    });

    it("should find the correct element by the given or query", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository(MockModel);
        const ids = [`first`, `second`, `third`];
        const mockElements: MockModel[] = ids.map((id, index) => ({
            id,
            foo: index === 0 ? "bar" : undefined,
        }));
        await repository.insert(mockElements);

        const conditionBuilder = database.getConditionBuilder<MockModel>();
        const query = repository.createQuery();
        const secondEqualOperator = conditionBuilder.createEqualOperator("id", "second");
        const thirdEqualOperator = conditionBuilder.createEqualOperator("foo", "bar");
        const orOperator = conditionBuilder.createOrOperator([secondEqualOperator, thirdEqualOperator]);
        query.addCondition(orOperator);

        const result = (await repository.findByQuery(query)).getResults();
        expect(result.length).toEqual(2);
    });

    it("should find the correct element by the given and query", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository(MockModel);
        const ids = [`first`, `second`, `third`];
        const mockElements: MockModel[] = ids.map(id => ({
            id,
            foo: "bar",
        }));
        await repository.insert(mockElements);

        const conditionBuilder = database.getConditionBuilder<MockModel>();
        const query = repository.createQuery();
        const secondEqualOperator = conditionBuilder.createEqualOperator("id", "second");
        const thirdEqualOperator = conditionBuilder.createEqualOperator("foo", "bar");
        const orOperator = conditionBuilder.createAndOperator([secondEqualOperator, thirdEqualOperator]);
        query.addCondition(orOperator);

        const result = (await repository.findByQuery(query)).getResults();
        expect(result.length).toEqual(1);
        expect(result[0].id).toEqual("second");
    });

    it("should find one element by the given query", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository(MockModel);
        const ids = [`first`, `second`, `third`];
        const mockElements: MockModel[] = ids.map((id, index) => ({
            id,
            foo: index % 2 === 1 ? "bar" : undefined,
        }));
        await repository.insert(mockElements);

        const conditionBuilder = database.getConditionBuilder<MockModel>();
        const query = repository.createQuery();
        query.addCondition(conditionBuilder.createEqualOperator("foo", "bar"));

        const result = (await repository.findOneByQuery(query)).getFirstResult();
        expect(result).not.toEqual(null);
        expect(result?.id).toEqual("second");
    });

    it("should find one element by the given query and return only the requested fields", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository<MockModel>(MockModel);
        const mockElement: MockModel = {
            id: "foo",
            foo: "bar",
        };
        await repository.insertOne(mockElement);

        const query = repository.createQuery<ProjectionModel>();
        query.setFields(["foo"]);
        const result = (await repository.findOneByQuery<ProjectionModel>(query)).getFirstResult();

        expect(result).toEqual({
            foo: "bar",
        });
    });
});
