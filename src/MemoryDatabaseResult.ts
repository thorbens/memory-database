import {AbstractDatabaseResult, DatabaseAggregationMap, ReadOnlyDatabaseQuery} from "@thorbens/database";

export class MemoryDatabaseResult<TSource, TResult = TSource> extends AbstractDatabaseResult<TSource, TResult> {
    public constructor(
        query: ReadOnlyDatabaseQuery<TSource, TResult>,
        results: TResult[],
    ) {
        super(query, results);
    }

    public getTotalSize(): Promise<number> {
        return Promise.resolve(this.getSize());
    }

    public async getAggregations(): Promise<DatabaseAggregationMap> {
        // not supported atm
        return Promise.resolve({});
    }

}
