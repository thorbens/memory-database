import {DatabaseModel} from "@thorbens/database";
import {MemoryDatabase} from "./MemoryDatabase";

class MockModel extends DatabaseModel {
}

describe("MemoryDatabase", () => {
    it("should return a repository for the given type", async () => {
        const database = new MemoryDatabase();
        const repository = await database.getRepository(MockModel);
        expect(repository).not.toEqual(null);
    });
});
