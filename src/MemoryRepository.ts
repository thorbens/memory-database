import {
    AbstractAtomDatabaseOperator,
    AbstractLogicalDatabaseOperator,
    AtomDatabaseOperator,
    CommonDatabaseQuery,
    CommonDatabaseOperatorType,
    DatabaseIDProvider,
    DatabaseModel,
    DatabaseOperator,
    DatabaseQuery,
    DatabaseResult,
    LogicalDatabaseOperator,
    ReadOnlyDatabaseQuery,
    Repository,
} from "@thorbens/database";
import {MemoryDatabaseResult} from "./MemoryDatabaseResult";

/**
 * Database implementation for a memory database.
 */
export class MemoryRepository<T extends DatabaseModel> implements Repository<T> {
    /**
     * The internal storage.
     */
    private readonly storage: Map<string, T> = new Map();

    public async insertOne(element: Readonly<T>): Promise<void> {
        this.storage.set(element.id, element);
    }

    public async insert(elements: ReadonlyArray<T>): Promise<void> {
        elements.map(this.insertOne.bind(this));

        await Promise.all(elements);
    }

    public async insertOrReplaceOne(element: Readonly<T>): Promise<void> {
        this.storage.set(element.id, element);
    }

    public async replaceOne(element: Readonly<T>): Promise<void> {
        this.storage.set(element.id, element);
    }

    public async updateOneField(
        id: string,
        field: keyof T,
        data: T[keyof T],
    ): Promise<void> {
        const entry = await this.findById(id) as any;
        if (entry == null) {
            throw new Error(`element with id ${id} does not exists`);
        }
        entry[field] = data;
        this.storage.set(id, entry);
    }

    public async updateOne(data: Partial<T> & DatabaseIDProvider): Promise<void> {
        const entry = await this.findById(data.id);
        if (entry == null) {
            throw new Error(`element with id ${data.id} does not exists`);
        }
        const mergedEntry = Object.assign(entry, data);

        this.storage.set(mergedEntry.id, mergedEntry);
    }

    public async update(query: ReadOnlyDatabaseQuery<T>, data: Partial<T>): Promise<void> {
        const results = (await this.findByQuery(query)).getResults();
        for (const result of results) {
            const mergedEntry = Object.assign(result, data);

            this.storage.set(mergedEntry.id, mergedEntry);
        }
    }

    public async findById(id: string): Promise<T & DatabaseIDProvider | null> {
        return this.storage.get(id) as any || null;
    }

    public async findByQuery<TResult = T>(query: ReadOnlyDatabaseQuery<T, TResult>): Promise<DatabaseResult<T, TResult>> {
        const conditions = query.getConditions();
        const elements = this.applyProjection<TResult>(query.getFields() as any, Array.from(this.storage.values()));

        return this.createDatabaseResult(query, this.applyConditions<TResult>(elements, conditions));
    }

    public async findOneByQuery<TResult = T>(
        query: ReadOnlyDatabaseQuery<T, TResult>,
    ): Promise<DatabaseResult<T, TResult>> {
        return this.findByQuery(query);
    }

    public createQuery<TResult = T>(): DatabaseQuery<T, TResult> {
        return new CommonDatabaseQuery<T, TResult>();
    }

    public async deleteById(id: string): Promise<void> {
        if (this.storage.has(id)) {
            this.storage.delete(id);
        }
    }

    public async deleteByQuery(query: ReadOnlyDatabaseQuery<T>): Promise<void> {
        const results = (await this.findByQuery(query)).getResults();

        await Promise.all(results.map(result => this.deleteById(result.id)));
    }

    public async count(query: ReadOnlyDatabaseQuery<T>): Promise<number> {
        const result = await this.findByQuery(query);

        return result.getSize();
    }

    public async getName(): Promise<string> {
        return this.constructor.name;
    }

    public async drop(): Promise<void> {
        this.storage.clear();
    }

    /**
     * Applies a projection to the given elements to keep only the given fields.
     *
     * @param fields The fields to keep.
     * @param elements The elements to apply to.
     */
    private applyProjection<TResult = T>(fields: ReadonlyArray<keyof T>, elements: ReadonlyArray<T>): TResult[] {
        if (!fields.length) {
            return elements as any;
        }

        return elements.map(element => fields.reduce((collector, field) => ({
            ...collector,
            [field]: element[field],
        }), {} as any));
    }

    /**
     * Applies the given conditions to the given elements.
     *
     * @param elements The elements to apply the conditions on.
     * @param conditions The conditions to apply.
     */
    private applyConditions<TResult = T>(
        elements: ReadonlyArray<TResult>,
        conditions: ReadonlyArray<DatabaseOperator>,
    ): TResult[] {
        return conditions.reduce(this.handleCondition.bind(this), [...elements]);
    }

    /**
     * Handles the given condition on the given elements.
     *
     * @param elements The element to apply the condition on/
     * @param condition The condition to apply.
     */
    private handleCondition<TResult = T>(
        elements: ReadonlyArray<TResult>,
        condition: DatabaseOperator,
    ): TResult[] {
        if (condition instanceof AbstractAtomDatabaseOperator) {
            return this.handleAtomicOperator(elements, condition as AtomDatabaseOperator<T>);
        } else if (condition instanceof AbstractLogicalDatabaseOperator) {
            return this.handleLogicOperator(elements, condition as LogicalDatabaseOperator<T>);
        }

        return [...elements];
    }

    /**
     * Filters the given element by the given condition.
     *
     * @param element The element to filter.
     * @param condition The condition to use for filtering.
     */
    private filterElementByOperator<TResult = T>(element: TResult, condition: DatabaseOperator): boolean {
        if (condition instanceof AbstractAtomDatabaseOperator) {
            return this.filterElementByAtomicOperator(element, condition as AtomDatabaseOperator<T>);
        } else if (condition instanceof AbstractLogicalDatabaseOperator) {
            return this.filterElementByLogicalOperator(element, condition as LogicalDatabaseOperator<T>);
        }

        return false;
    }

    /**
     * Handles an atomic operator on the given elements.
     *
     * @param elements The element to apply the operator on.
     * @param operator The operator to apply.
     */
    private handleAtomicOperator<TResult = T>(
        elements: ReadonlyArray<TResult>,
        operator: AtomDatabaseOperator<T>,
    ): TResult[] {
        return elements.filter(element => this.filterElementByAtomicOperator(element, operator));
    }

    /**
     * Filter the given element by the given atomic operator.
     *
     * @param element The element to filter.
     * @param operator The operator to use for filtering.
     */
    private filterElementByAtomicOperator<TResult = T>(
        element: TResult,
        operator: AtomDatabaseOperator<T>,
    ): boolean {
        const key = operator.getKey() as keyof TResult;
        switch (operator.getType()) {
            case CommonDatabaseOperatorType.EQUAL:
                return element[key] && element[key] === operator.getValue();
            case CommonDatabaseOperatorType.NOT_EQUAL:
                return element[key] && element[key] !== operator.getValue();
            case CommonDatabaseOperatorType.GT:
                return element[key] && element[key] > (operator.getValue() as never);
            case CommonDatabaseOperatorType.LT:
                return element[key] && element[key] < (operator.getValue() as never);
            case CommonDatabaseOperatorType.REGEXP:
                const regExp = operator.getValue() as RegExp;

                return element[key] && regExp.test(`${element[key]}`);
        }

        return false;
    }

    /**
     * Handles a logical operator.
     *
     * @param elements The element to apply the operator on.
     * @param operator The operator to apply.
     */
    private handleLogicOperator<TResult = T>(
        elements: ReadonlyArray<TResult>,
        operator: LogicalDatabaseOperator<TResult>,
    ): TResult[] {
        return elements.filter(element => this.filterElementByLogicalOperator(element, operator));
    }

    /**
     * Filters the given element by the given logical operator.
     *
     * @param element The element to filter.
     * @param operator The operator to use for filtering.
     */
    private filterElementByLogicalOperator<TResult = T>(
        element: TResult,
        operator: LogicalDatabaseOperator<TResult>,
    ): boolean {
        switch (operator.getType()) {
            case CommonDatabaseOperatorType.OR:
                return operator.getValue().some(condition => this.filterElementByOperator(element, condition));
            case CommonDatabaseOperatorType.AND:
                return operator.getValue().every(condition => this.filterElementByOperator(element, condition));
        }

        return false;
    }

    private createDatabaseResult<TSource, TResult = TSource>(
        query: ReadOnlyDatabaseQuery<TSource, TResult>,
        results: TResult[],
    ) {
        return new MemoryDatabaseResult(query, results);
    }
}
